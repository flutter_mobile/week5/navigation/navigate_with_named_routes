import 'dart:js';

import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Named Route Demo",
    initialRoute: "/",
    routes: {
      "/": (context) => const FirstScreen(),
      "/second": (context) => const SecondScreen(),
      "/third": (context) => const ThirdScreen(),
    },
  ));
}

class FirstScreen extends StatelessWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("First Screen"),
      ),
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 10.0,
            ),
            ElevatedButton(
              child: const Text("Launch screen Second"),
              onPressed: () {
                Navigator.pushNamed(context, "/second");
              },
            ),
            SizedBox(
              height: 10.0,
            ),
            ElevatedButton(
              child: const Text("Launch screen Third"),
              onPressed: () {
                Navigator.pushNamed(context, "/third");
              },
            ),
          ],
        ),
      ),
    );
  }
}

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Screen"),
      ),
      body: Center(
        child: ElevatedButton(
          child: const Text("Go back"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}

class ThirdScreen extends StatelessWidget {
  const ThirdScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Third Screen"),
      ),
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 10.0,
            ),
            ElevatedButton(
              child: const Text("Go back Second Screen"),
              onPressed: () {
                Navigator.pushNamed(context, "/second");
              },
            ),
            SizedBox(
              height: 10.0,
            ),
            ElevatedButton(
              child: const Text("Go back"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
